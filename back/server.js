var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

//variable para poder usar el request-json
var requestJson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

//bloque para conectar a un mongodb local
var mongoClient = require('mongodb').MongoClient;
//locashost se debería cambiar por el nombre del contenedor donde se ejecuta
//var url = "mongodb://localhost:27017/local";
var url = "mongodb://servermongo:27017/local";

//para permitir cors
var cors = require('cors')
app.use(cors())

var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?apiKey=IlW-Ff43eQXC2j5Inxnf-Hsys-J3pZ4e";
var clienteMlab = requestJson.createClient(urlmovimientosMlab);

//Para usar las librerias de Postgre
var pg = require('pg');
//url para conectarnos al contenedor de la bbdd postgre
//cambiar el localhost por el nombre del contenedor que le hemos dado al lanzarlo cuando se ejecute desde el contenedor
var urlUsuarios = "postgres://docker:docker@localhost:5432/bdseguridad";
//var urlUsuarios = "postgres://docker:docker@serverpostgre:5432/bdseguridad";
var clientePostgre = new pg.Client(urlUsuarios);
//asumo que recibo algo así: req.body = {login:xxx, password:yyy}


app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/movimientos2', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    mongoClient.connect(url, function(err, db) {
      if (err)
      {
        console.log(err);
      }
      else {
        console.log("Connected succesfully to serve" + url);
        var col = db.collection('movimientos');
        col.find({}).limit(3).toArray(function(err, docs) {
          res.send(docs);
        });
        db.close();
      }
    })
});

app.post('/movimientos2', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    mongoClient.connect(url, function(err, db) {
      var col = db.collection('movimientos');
      console.log("trying to insert to server");
      col.insertOne({a:1}, function(err, r) {
        console.log(r.insertedCount + 'registros insertados');
      });
/*
      //insert multiple documents
      col.insertMany([{a:2}, {a:3}], function(err, r){
        console.log(r.insertedCount + ' registros insertados');
      });
      //insert con body
      col.insert(req.body, function(err, r) {
        console.log(r.insertedCount + ' registros insertados body');
      });
*/
        db.close();
        res.send("ok");
    })
});

app.post('/movimientos3', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    mongoClient.connect(url, function(err, db) {
      var col = db.collection('movimientos');
      console.log("trying to insert to server");
/*
      col.insertOne({a:1}, function(err, r) {
        console.log(r.insertedCount + 'registros insertados');
      });

      //insert multiple documents
      col.insertMany([{a:2}, {a:3}], function(err, r){
        console.log(r.insertedCount + ' registros insertados');
      });
*/
      //insert con body
      col.insert(req.body, function(err, r) {
        console.log(r.insertedCount + ' registros insertados body');
      });

        db.close();
        res.send("ok");
    })
});

app.get('/movimientos', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.get('/clientes', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab.get('&f={"idcliente":1, "nombre": 1, "apellidos": 1}', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.post('/movimientos', function(req, res) {
  /* Crear movimiento en MLab */
//  var clienteMlab = requestJson.createClient(urlMovimientosMlab);
/*  //metiendo a pelo el body de la petición post
  var reqBody = {
              	"nombre": "Juan Luis",
              	"apellido": "POST node",
              	"idcliente": 1234
              };
*/
  //cogiendo el body que viene en la petición
  console.log(req.body);
  var reqBody = req.body;

  clienteMlab.post('', reqBody, function(err, resM, body) {
    if(err)
    {
      console.log(body);
    }
    else {
      res.send(body);
    }
  });
});

app.get('/movimientos/:idcliente', function (req, res) {
  /* Obtener movimientos del cliente idcliente desde MLab */
/*
  var id = req.params.idcliente; //o req.query
    if (id<2){
      res.send('Usuario: ' + clientes[id].user + ' Pass: ' + clientes[id].pass);
    } else {
      res.send('Introduce cliente 0 o 1');
    }
*/
})

/*
//el put para Mlab
app.put('/movimientos', function(req,res){
  var clienteMlab = requestJson.createClient(urlMovimientosMlab);
  //metiendo a pelo el body
  var reqBody = {
	   "$set": { "apellido":"JSONmodificado2"}
  };
  //metiendo a pelo el querystring
  clienteMlab.put('&q={"idcliente":8888}&m=true', reqBody, function(err, resM, body) {
    if(err)
    {
      console.log(body);
    }
    else {
      res.send(body);
    }
  });
});
*/

app.post('/login', function(req, res) {
  console.log("entramos");
  //Crear cliente PostgreSQL
  clientePostgre.connect();
  //asumo que recibo algo así: req.body = {login:xxx, password:yyy}
  //Hacer consulta
  const query = clientePostgre.query('SELECT COUNT(*) FROM usuarios WHERE login=$1 AND password=$2;',[req.body.login, req.body.password], (err, result) => {
    if (err) {
      console.log(err);
      res.send(err);
    }
    else {
      if (result.rows[0].count >= 1)
      {
        res.send("Login correcto");
      }
      else {
        res.send("Login incorrecto");
      }
      /*
      // se está asumiendo que sólo viene un resultado, si viniera más o menos habría que comprobarlo.
      console.log(result.rows[0]);
      res.send(result.rows[0]);*/
 }  });


  //Devolver  resultado

});
